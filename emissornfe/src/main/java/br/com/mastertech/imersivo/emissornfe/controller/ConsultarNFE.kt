package br.com.mastertech.imersivo.emissornfe.controller

import br.com.mastertech.imersivo.kafka.models.dto.RsConsultaNFE
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "consultadoc")
interface ConsultarNFE {

    @GetMapping("/nfe/consultar/{cpf_cnpj}")
    fun consulta(@RequestParam(name = "cpf_cnpj") doc: String) : List<RsConsultaNFE>?
}