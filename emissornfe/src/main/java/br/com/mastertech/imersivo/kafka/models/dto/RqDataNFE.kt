package br.com.mastertech.imersivo.kafka.models.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class RqDataNFE(@JsonProperty("identidade") val identidade: String
                , @JsonProperty("valor")  val valor: Double)