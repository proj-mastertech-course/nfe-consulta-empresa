package br.com.mastertech.imersivo.kafka.models.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class RsEmissaoNFE(@JsonProperty("status") val status: String = "pending")